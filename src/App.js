import HeroSection from "components/HeroSection";
import AboutSection from "components/AboutSection";
import ExperienceSection from "components/ExperienceSection";
import PortfolioSection from "components/PortfolioSection";
import ContactSeciton from "components/ContactSeciton";
import FooterSeciton from "components/FooterSeciton";

function App() {
  return (
    <>
      <HeroSection />
      <AboutSection />
      <ExperienceSection />
      <PortfolioSection />
      <ContactSeciton />
      <FooterSeciton />
    </>
  );
}

export default App;
