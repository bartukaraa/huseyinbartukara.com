import React from "react";

export default function AboutSection() {
  const handleDownloadCV = () => {
    // İndirilecek dosyanın URL'si
    const downloadURL = "/assets/cv/huseyinbartukara.pdf";

    // Dosyayı indirmek için bir link oluşturuluyor
    const link = document.createElement("a");
    link.href = downloadURL;
    link.download = "Huseyin_Bartu_Kara_CV.pdf";

    // Link tetikleniyor ve indirme işlemi gerçekleşiyor
    link.click();
  };

  const handleContact = () => {
    // E-posta adresi
    const emailAddress = "huseyinbartukara@gmail.com";

    // E-posta bağlantısı oluşturuluyor
    const mailtoLink = `mailto:${emailAddress}`;

    // E-posta uygulamasını açmak için link tetikleniyor
    window.location.href = mailtoLink;
  };

  return (
    <>
      <div
        id="aboutSection"
        className="container mx-auto bg-white-color text-center py-10 my-8"
      >
        {/* Hakkımda başlığı */}

        {/* İçerik */}
        <div className="flex flex-wrap mt-8">
          {/* Resim */}
          <div className="w-full md:w-1/4 p-4 ">
            <img
              src="/assets/images/generals/ben.jpeg"
              alt="Profil Resmi"
              className="mx-auto rounded-lg w-80"
            />
          </div>

          {/* Metin ve Butonlar */}
          <div className="w-full md:w-3/4  flex flex-col justify-center items-start px-8">
            <h2 className="text-3xl font-semibold">Hakkımda</h2>
            {/* Metin */}
            <p className="text-lg">
              31 Ağustos 2000'de Kahramanmaraşta doğdum. İlköğretim dönemimi
              İzmir'de tamamlamın ardından Denizli'de lise hayatıma başladım.
              Lise hayatımda hobi projeleri amacıyla başladığım geliştiricilik
              hayatıma 2018 Isparta Süleyman Demirel Üniversitesi Bilgisayar
              Mühendisliği bölümünü kazanarak devam ettim. İlk yılımda ingilizce
              hazırlık eğitimimi tamamladıktan sonra alan eğitimleriminde
              başlamasıyla c++ ve java dillerine yöneldim. Üniversitede ki ilk
              senemin sonunda kendime bir alan belirlemeli ve o alanda
              uzmanlaşmam gerektiğini düşünerek zaten hali hazırda basit düzeyde
              hakim olduğum java dilini kullanarak android uygulama geliştirmeye
              karar verdim. Sonrasında Kotlin programlama dilini merak sarıp
              android kariyerime Kotlin ile devam ettim. Bir stajımı kotlin
              programlama dilini kullanan bir şirkette geçirdikten sonra iOS
              geliştirme dünyasını denemek istediğime karar verdim ve Swift -
              SwiftUI kullanarak geliştirmelerime devam ettim. Bu aşamada iki
              farklı şirkette gönüllü olarak iOS geliştirme pozisyonunda
              çalıştım. Okulumun bitmesine yakın bir dönemde React ile tanıştım
              ve kariyerime bu yolda devam ettim. Şu anda aktif olarak bir
              şirkette Front-End geliştirici olarak görev yapıyorum. Kariyerim
              boyunca geliştirdiğim projelerimi veya deneyimlerimi görmek için
              lütfen ekranı kaydırmaya devam edin!
            </p>

            {/* Butonlar */}
            <div className="flex mt-4 w-full justify-end">
              <button
                className="bg-blue-500 text-white border-2 border-blue-opacity-color hover:border-live-blue-color  px-4 py-2 rounded-3xl mr-4"
                onClick={handleDownloadCV}
              >
                CV İndir
              </button>
              <button
                className="bg-blue-500 text-white border-2 border-blue-opacity-color hover:border-live-blue-color px-4 py-2 rounded-3xl"
                onClick={handleContact}
              >
                İletişim
              </button>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
