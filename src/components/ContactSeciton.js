import React from "react";
import { BsTelephoneInboundFill } from "react-icons/bs";
import { IoMdMail } from "react-icons/io";
import { BsGithub } from "react-icons/bs";

export default function ContactSeciton() {
  return (
    <div id="contactSection" className="container mx-auto py-12">
      <div className="mb-8">
        <h1 className="text-2xl font-bold mb-4 text-center">İletişim</h1>
        <p className="text-gray-600 text-center">
          Projen için ücret teklifi almak, aklına takılan bir soruyu sormak ya
          da "Merhaba!" demek için mail adreslerimden bana ulaş, sosyal medya
          adreslerimden bana yaz, hem kendimi geliştirmek hem de yeni insanlarla
          tanışmak için burdayım.
        </p>
      </div>
      <div className="grid grid-cols-1 md:grid-cols-3 gap-4 px-8">
        <div className="text-center border-2 border-primary-brand-color py-10 rounded-xl transition duration-300 transform hover:scale-105 hover:shadow-lg whitespace-normal">
          <BsGithub className="w-6 h-6 mx-auto mb-4" />
          <a href="https://gitlab.com/bartukaraa">
            <p className="text-lg font-serif hover:underline">Gitlab</p>
          </a>
        </div>
        <div className="text-center border-2 border-secondary-brand-color py-10 rounded-xl transition duration-300 transform hover:scale-105 hover:shadow-lg whitespace-normal">
          <BsTelephoneInboundFill className="w-6 h-6 mx-auto mb-4" />
          <p className="text-lg">0532 465 7037</p>
        </div>
        <div className="text-center border-2 border-primary-brand-color py-10 rounded-xl transition duration-300 transform hover:scale-105 hover:shadow-lg whitespace-normal">
          <IoMdMail className="w-6 h-6 mx-auto mb-4" />
          <p className="text-lg font-serif">huseyinbartukara@gmail.com</p>
        </div>
      </div>
    </div>
  );
}
