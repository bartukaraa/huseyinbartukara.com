import React from "react";
import Timeline from "./ui/Timeline";

const ExperienceSection = () => {
  const timelineItems = [
    {
      date: "02.01.2023 - 06.02.2023",
      title: "Kodpit Teknoloji A.Ş",
      description:
        "Github hesabımda da bulabiliceginiz StajyerBul uygulamasını zorunlu staj kapsamında geliştirdim. Bu uygulamayı geliştirirken Kotlin kodlama dilini kullandım. Uygulamanının ana amacı Pamukkale Üniversitesi teknokent içerisinde bulunan şirketlerin stajyer bulma sürecini kolaylaştırmak ayrıca şirket ve öğrenci arasında ki bağlantıyı mobil arayüze taşımak. Uygulama bir demo uygulama olarak tasarlanıp geliştirilse de kodlara github hesabım üzerinden erişebilirsiniz.",
    },
    {
      date: "11.10.2023 - 11.12.2023",
      title: "Supafo",
      description:
        "Kariyerime Swift geliştiricisi olarak devam ettiğim dönemde kendimi geliştirmek adına gönüllü geliştirme projeleri araştırıyordum. Bu sırada Supafo ile tanıştım ve bir ay olmak üzere gönüllü olarak swiftUI framework kullanarak geliştirmeler yaptım. Uygulama aslında bir tasarruf uygulaması olarak planlanmıştı, anlaşmalı kurumların belirli krıterleri karşılayan ürünlerini daha uygun fiyata satıcı ile buluşturmasını hedefliyordu. Bir ay gibi kısa bir sürede ben ve çalıştığım ekip iOS alanında ki geliştirmelerini tamamlamış ve test aşaması için uygulamayı hazır hale getirmişti. Swift kariyerim ve benim için önemli bir dönüm noktası olan bu çalışma takım çalışması, problem çözme, remote çalışma yapısı ve güncel kalma konusunda bana çok şey kattı.",
    },
    {
      date: "15.11.2023 - Devam Ediyor",
      title: "Bia Yazılım",
      description:
        "Şu anda da aktif olarak çalışma hayatıma tam zamanlı olarak devam ettiğim şirketim Bia yazılım. Şirketimle staj vasıtasıyla tanıştım diyebilirim bahsettiğim tarihler arasında React-Native geliştiricisi alanında stajyer olarak Bia yazılım yolculuğuma başladım ve staj sürecim boyunca bir adet uygulama çıkardım bu uygulamanın adı Bee PDKS uygulama bir personel denetim kontrol sistemi, ilk uygulamamın ardından staj süremin sonuna geldim ve karşışıklı anlaşarak profesyonel hayata ilk adımımı attım, ikinci projem olarak Mermercim.com projesini hayata geçirdim. Bu uygulama bir mermer e-ticaret uygulaması. 3 farklı modülden oluşan uygulama React-Native kullanılarak geliştirildi ve şu anda aktif olarak mobil marketlerde yerini almakta. Şu anki kariyerim React geliştiricisi olma yolunda devam ediyor. React ve Tailwind kullanarak şirket için web geliştirmeler yapıyorum geliştirdiğim ve canlıya alınan  projelerime portföy bölümünden ulaşabilirsiniz.",
    },
    // Diğer iş deneyimlerini buraya ekleyin
  ];
  return (
    <div id="experienceSection" className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4 text-center">Deneyimlerim</h1>
      <Timeline items={timelineItems} />
    </div>
  );
};

export default ExperienceSection;
