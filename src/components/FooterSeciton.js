import React from "react";
import {
  FaFacebookF,
  FaTwitter,
  FaInstagram,
  FaLinkedinIn,
} from "react-icons/fa";
import { FiMail } from "react-icons/fi";

export default function FooterSeciton() {
  return (
    <footer className="bg-primary-brand-color text-white py-8">
      <div className="container mx-auto flex items-center justify-center md:justify-between">
        {/* Sol tarafta yer alan ikon */}
        <div className=" items-center hidden md:flex">
          <img
            src="assets/images/icons/navBarIcon.png"
            alt="Logo"
            className=" w-8 h-8"
          />
          <h2 className="text-lg font-semibold ml-2 text-white-color">
            Hüseyin Bartu Kara
          </h2>
        </div>
        {/* Sağ tarafta yer alan sosyal medya ikonları */}
        <div className="flex items-center">
          <a href="https://twitter.com/bartukaraa" className="mr-4">
            <FaTwitter className="text-white-color text-2xl hover:text-live-blue-color transition duration-300" />
          </a>
          <a href="https://www.instagram.com/bartu_karaa/" className="mr-4">
            <FaInstagram className="text-white-color text-2xl hover:text-live-blue-color transition duration-300" />
          </a>
          <a href="https://www.linkedin.com/in/huseyinbartukara/">
            <FaLinkedinIn className="text-white-color text-2xl hover:text-live-blue-color transition duration-300" />
          </a>
        </div>
      </div>
    </footer>
  );
}
