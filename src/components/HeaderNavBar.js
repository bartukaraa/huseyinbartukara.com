import React from "react";
import { Link } from "react-scroll";

export default function Header() {
  return (
    <div className="  top-0 z-50 static">
      <div className="container mx-auto px-6 h-12 flex items-center  justify-between ">
        <Link
          to="heroSection"
          smooth={true}
          duration={500}
          className="cursor-pointer flex flex-row"
        >
          <img
            src="assets/images/icons/navBarIcon.png"
            alt="Logo"
            className=" w-8 h-8"
          />
          <h2 className="text-lg font-semibold ml-2">Hüseyin Bartu Kara</h2>
        </Link>
        <nav className=" hidden md:flex gap-x-6 text-sm font-semibold items-center">
          <Link
            to="heroSection"
            smooth={true}
            duration={500}
            className=" cursor-pointer text-opacity-80 hover:text-opacity-100 hover:text-primary-brand-color transition-all flex items-start gap-x-2"
          >
            Anasayfa
          </Link>
          <Link
            to="aboutSection"
            smooth={true}
            duration={500}
            className=" cursor-pointer text-opacity-80 hover:text-opacity-100 hover:text-primary-brand-color transition-all flex items-start gap-x-2"
          >
            Hakkımda
          </Link>
          <Link
            to="experienceSection"
            smooth={true}
            duration={500}
            className=" cursor-pointer text-opacity-80 hover:text-opacity-100 hover:text-primary-brand-color transition-all flex items-start gap-x-2"
          >
            Deneyimlerim
          </Link>
          <Link
            to="portfolyoSection"
            smooth={true}
            duration={500}
            className=" cursor-pointer text-opacity-80 hover:text-opacity-100 hover:text-primary-brand-color transition-all flex items-start gap-x-2"
          >
            Portfolyo
          </Link>

          <Link
            to="contactSection"
            smooth={true}
            duration={500}
            className=" cursor-pointer text-opacity-80 hover:text-opacity-100 hover:text-primary-brand-color transition-all flex items-start gap-x-2"
          >
            İletişim
          </Link>
        </nav>
      </div>
    </div>
  );
}
