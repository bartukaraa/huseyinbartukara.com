import React, { useEffect, useState } from "react";
import Header from "./HeaderNavBar";
import Lottie from "react-lottie";
import animationData from "animation/heroAnimation.json";
import Typewriter from "typewriter-effect";
import { MdKeyboardDoubleArrowDown } from "react-icons/md";
import { motion } from "framer-motion";
import { Link } from "react-scroll";

export default function HeroSection() {
  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData, // JSON dosyasının içeriği
    rendererSettings: {
      preserveAspectRatio: "xMidYMid slice",
    },
  };

  return (
    <div
      id="heroSection"
      className="h-screen bg-blue-opacity-color flex flex-col"
    >
      {/* Header */}
      <Header className="h-20" />

      {/* Ana içerik */}
      <div className="flex-grow bg-blue-opacity-color flex flex-col justify-center items-center">
        <div className="flex-grow bg-blue-opacity-color flex justify-center items-center">
          <div className="w-1/2  flex justify-center items-center text-7xl md:text-8xl font-semibold">
            {/* Sağ tarafta yer alacak metin */}
            <Typewriter
              options={{
                strings: [
                  "Hello <br> World!",
                  "Front-End",
                  "React",
                  "Tailwind",
                  "Junior Developer",
                ],
                autoStart: true,
                loop: true,
                html: true, // HTML etiketlerini işleme almak için html: true özelliğini ayarlayın
              }}
            />
          </div>
          <div className="w-1/2 p-4 md:block hidden">
            {/* Sol tarafta yer alacak animasyon */}
            <Lottie options={defaultOptions} />
          </div>
        </div>
        <div className="mt-auto">
          {/* En altta yer alacak buton */}
          <div className="mt-auto">
            {/* En altta yer alacak buton */}
            <Link
              to="aboutSection"
              smooth={true}
              duration={500}
              className=" cursor-pointer text-opacity-80 hover:text-opacity-100 hover:text-primary-brand-color transition-all flex items-start gap-x-2"
            >
              <motion.button
                className="bg-gray-700 text-white px-4 py-2 rounded-md flex items-center"
                onClick={() => {
                  // Butona tıklandığında yapılacak işlemler
                }}
                animate={{
                  y: [0, -10, 0], // Y eksenindeki hareketler: normal, yukarı, normal
                }}
                transition={{
                  y: {
                    repeat: Infinity, // Sonsuz tekrar
                    duration: 0.5, // Hareket süresi (saniye cinsinden)
                  },
                }}
              >
                <MdKeyboardDoubleArrowDown className="ml-2 text-3xl text-secondary-brand-color" />
              </motion.button>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}
