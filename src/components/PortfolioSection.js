import React from "react";

export default function PortfolioSection() {
  return (
    <div id="portfolyoSection" className="container mx-auto">
      <h1 className="text-2xl font-bold mb-4 text-center">Portfolyom</h1>
      <div className="flex flex-col md:grid md:grid-cols-3 gap-3">
        <a
          href="https://apps.apple.com/tr/app/mermercim/id6478965434"
          className="relative rounded overflow-hidden mx-4"
        >
          <img
            src="/assets/images/mockUps/mermercimMobile.png"
            alt="Hanging Planters"
            className="w-full"
          />
        </a>
        <div className="relative rounded overflow-hidden mx-4">
          <img
            src="/assets/images/mockUps/mermercimAdminMobile.png"
            alt="Planter Stand with Pots"
            className="w-full"
          />
        </div>
        <a
          href="https://apps.apple.com/tr/app/bee-pdks/id6477382882"
          className="relative rounded overflow-hidden mx-4"
        >
          <img
            src="/assets/images/mockUps/beePdksMobile.png"
            alt="Watering Cans"
            className="w-full"
          />
        </a>
        <a
          href="https://www.beepdks.com/"
          className="relative rounded overflow-hidden mx-4"
        >
          <img
            src="/assets/images/mockUps/beePdksWeb.png"
            alt="Metal Planters"
            className="w-full"
          />
        </a>
        <a
          href="https://bsys.canondenizli.com/"
          className="relative rounded overflow-hidden mx-4 "
        >
          <img
            src="/assets/images/mockUps/servisTakip2.png"
            alt="Metal Planters"
            className="w-full"
          />
        </a>
        <a
          href="https://servis.fillover.com"
          className="relative rounded overflow-hidden mx-4 "
        >
          <img
            src="/assets/images/mockUps/fillOver.png"
            alt="Metal Planters"
            className="w-full"
          />
        </a>
      </div>
    </div>
  );
}
