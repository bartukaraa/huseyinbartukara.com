import React from "react";
import TimelineItem from "./TimelineItem";

const Timeline = ({ items }) => {
  return (
    <div>
      {items.map((item, index) => (
        <TimelineItem key={index} {...item} />
      ))}
    </div>
  );
};

export default Timeline;
