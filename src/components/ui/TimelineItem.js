import React from "react";

const TimelineItem = ({ date, title, description, link, linkTitle }) => {
  return (
    <div className="flex mb-6">
      <div className="flex-grow">
        <div className="flex justify-between">
          <div className="font-medium text-xl text-gray-900">{title}</div>
          <div className="text-sm text-gray-500">{date}</div>
        </div>
        <div className="text-sm text-gray-500 text-justify">{description}</div>
        <a href={link} className="text-primary-brand-color underline">
          {linkTitle}
        </a>
        <hr className="mt-2" />
      </div>
    </div>
  );
};

export default TimelineItem;
