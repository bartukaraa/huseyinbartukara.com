/** @type {import('tailwindcss').Config} */
module.exports = {
  mode: "jit",
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./public/index.html"],
  theme: {
    extend: {
      0.1: "0.063rem",
    },
    colors: {
      "primary-brand-color": "#112D4E",
      "secondary-brand-color": "#3F72AF",
      "white-color": "#FFFFFF",
      "blue-opacity-color": "#DBE2EF",
      "live-blue-color": "#00C6FF",
      "live-orange-color": "#F05A2B",
    },
  },
  plugins: [],
};
